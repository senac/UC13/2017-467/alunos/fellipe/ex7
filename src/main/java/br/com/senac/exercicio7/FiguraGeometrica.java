package br.com.senac.exercicio7;

public abstract class FiguraGeometrica {

    public abstract double getArea();

}
