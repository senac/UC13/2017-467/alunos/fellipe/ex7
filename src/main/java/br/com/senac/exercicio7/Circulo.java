package br.com.senac.exercicio7;

public class Circulo extends FiguraGeometrica {

    private double raio;

    public Circulo() {
    }

    public Circulo(double raio) {
        this.raio = raio;
    }

    @Override
    public double getArea() {
        return this.raio * 3.14;
    }

}
