package br.com.senac.exercicio7;

public class Quadrado extends FiguraGeometrica {

    private double lado1;

    public Quadrado(double lado1) {
        this.lado1 = lado1;
    }
  

    @Override
    public double getArea() {
        return this.lado1 * lado1;
    }

}
