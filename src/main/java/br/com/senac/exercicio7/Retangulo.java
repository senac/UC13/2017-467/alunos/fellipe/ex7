package br.com.senac.exercicio7;

public class Retangulo extends FiguraGeometrica {

    private double base;
    private double altura;

    public Retangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    @Override
    public double getArea() {
        return this.base * altura;
    }

}
