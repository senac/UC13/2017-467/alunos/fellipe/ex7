package br.com.senac.exercicio7;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class CalcularAreaTeste {

    public CalcularAreaTeste() {
    }

    @Test
    public void areaQuadrado() {

        Quadrado areaQuadrado = new Quadrado(10);

        assertEquals(100, areaQuadrado.getArea(), 0.01);

    }

    @Test
    public void areaTriangulo() {

        Triangulo areaTriangulo = new Triangulo(10, 10);

        assertEquals(50, areaTriangulo.getArea(), 0.01);
    }

    @Test
    public void areaRetangulo() {

        Retangulo areaRetangulo = new Retangulo(10, 10);

        assertEquals(100, areaRetangulo.getArea(), 0.01);

    }
    
    @Test
    public void areaCirculo(){
        Circulo areaCirculo = new Circulo(10);
        
        assertEquals(31.4, areaCirculo.getArea(), 0.01);
        
    }
    
    
    
    
    
    

}
